<?php
defined ('_JEXEC') or die('Resitriced Access');

jimport( 'joomla.plugin.plugin' );

class plgSystemSmartseo extends JPlugin
{
        /**
         * Constructor
         *
         * For php4 compatability we must not use the __constructor as a constructor for plugins
         * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
         * This causes problems with cross-referencing necessary for the observer design pattern.
         *
         * @access      protected
         * @param       object  $subject The object to observe
         * @param       array   $config  An array that holds the plugin configuration
         * @since       1.0
         */
        function plgSystemCache( &$subject, $config )
        {
                parent::__construct( $subject, $config );

                // Do some extra initialisation in this constructor if required
        }


		function onAfterRender() {
			$mainframe =& JFactory::getApplication('site'); //instance mainframe
			if (!$mainframe->isAdmin()) { 
			

		//display on frontend only 
			$phpURL = http_build_query($_GET);
			$fullURL = $_SERVER["REQUEST_URI"];
			//$FullURL =& JFactory::getURI();
			//$url = $FullURL->toString();
			//$uquery =&JURI::getInstance($url);
			//$ExtendedQuery = $uquery->getQuery();
			//if (strlen($ExtendedQuery)>0) { $url = $uquery->getPath() ."?" .$uquery->getQuery(); }
			//else { $url = $uquery->getPath(); }
			
			
			
		//DB Instance
		$doc =& JFactory::getDocument();
		$db =& JFactory::getDBO();
		//retrive saved seo information for current page.
		 global $mainframe;
		 $lights = 0;
		$query = "SELECT keywords, description, title, url FROM #__easyseo where url='$phpURL'";
		$db->setQuery($query);
		$data = $db->loadRow();
		if (!$data) {
				$title = "";
				$keywords = "";
				$description = "";
				$dburl = "";
		}
		else {
				if (strlen($data[0])>0) { $keywords = $data[0];$lights = $lights +1; } else { $keywords = ""; }
				if (strlen($data[1])>0) { $description = $data[1];$lights = $lights +1; } else { $description = ""; }
				if (strlen($data[2])>0) { $title = $data[2]; $lights = $lights +1; } else { $title = ""; }
				if (strlen($title) == 0) { $title = $doc->getTitle();}
				$title = str_replace('"','&quot;',$title);
				if (isset($data[3]) && strlen($data[3])>0) { $dburl = $data[3]; }
		}	
				//calc amount of green lights
				
/*				if (strlen($keywords)>0) { $lights = $lights +1;}
				if (strlen($description)>0) { $lights = $lights +1;}*/
				
				$lightswidth = $lights * 16;
  			
			$repl = "
			
			<div id=\"es_bar_container\">
			<div id=\"es_bar_hidden\">
				<form action=\"" .$fullURL ."\" method=\"POST\" name=\"easyseo_form\">
				<div class=\"es_bar_1\">
						<input name=\"seo_title\" type=\"text\" value=\"" .$title ."\" />
				</div>
				<div class=\"es_bar_2\">
					
					<textarea name=\"seo_description\" 
						onKeyDown=\"textCounter(document.easyseo_form.seo_description,document.easyseo_form.remLen1)\"
						onKeyUp=\"textCounter(document.easyseo_form.seo_description,document.easyseo_form.remLen1)\">".$description ."</textarea>
					<input readonly type=\"text\" name=\"remLen1\" id=\"description_count\">
				</div>
				<div class=\"es_bar_3\">
					
					<textarea name=\"seo_keywords\"
						onKeyDown=\"textCounter(document.easyseo_form.seo_keywords,document.easyseo_form.remLen2)\"
						onKeyUp=\"textCounter(document.easyseo_form.seo_keywords,document.easyseo_form.remLen2)\">".$keywords ."</textarea>
					<input readonly type=\"text\" name=\"remLen2\" id=\"keywords_count\">
				</div>
				<input type=\"hidden\" name=\"seo_post\" value=\"1\" />";
				if (strlen($dburl)>0) { $repl .= "<input type=\"hidden\" name=\"seo_update\" value=\"1\" />";}
				$repl .= "<input name=\"easyseo_submit\" type=\"submit\" value=\"Apply\" />
				</form>
			</div>
			
				<div id=\"es_bar\" onclick=\"javascript:metaKeywords('keywords');
				javascript:metaDescription('description');
				visibility_smart('es_bar_hidden');
				textCounter(document.easyseo_form.seo_description,document.easyseo_form.remLen1);
				textCounter(document.easyseo_form.seo_keywords,document.easyseo_form.remLen2);
				\">
					<div class=\"es_bar_left\">
						<form id=\"form-login\" name=\"login\" method=\"post\" action=\"index.php?option=com_users&view=login\">
							<input type=\"submit\" value=\"Log out\" class=\"button\" name=\"Submit\">
						</form>
					</div>
					
						<div class=\"es_bar_right\">
							<div id=\"glights\">
								<div id=\"lights\" style=\"width:$lightswidth" ."px;\"></div>
							</div>
						</div>
					
					<div style=\"clear:both;\"></div>
				</div>
			
			</div>";
			$plugin =&JPluginHelper::getPlugin('system','smartseo');
			//$params = new JParameter($plugin->params);
			//$user =& JFactory::getUser();
							$user =& JFactory::getUser();
				$userid = $user->id;
				$AdminCatID = $this->params->get('access_level');

				$query = "SELECT group_id FROM #__user_usergroup_map WHERE user_id = $userid";
				$db->setQuery($query);
				$DBAdminCatID = $db->loadResult();
					if ($AdminCatID == $DBAdminCatID) {
						$doc->addStyleSheet("plugins/system/smartseo/easyseo/style.css");
						$doc->addScript("plugins/system/smartseo/easyseo/toggleid.js");
					
			
							
						$body = JResponse::getBody();
						if (strpos($body,"</body>")) {
							$content = str_replace("</body>",$repl ."</body>",$body);
						}
						else {
							$content = str_replace("</html>",$repl ."</body></html>",$body);
						}
						JResponse::setBody($content);
				}
				
			}
		}
			
		
		
        function onAfterDispatch() {
			$doc =& JFactory::getDocument();
			$mainframe =& JFactory::getApplication('site'); //instance mainframe
				if (!$mainframe->isAdmin()) { 
			
				//get currect URL
/*				$FullURL =& JFactory::getURI();
				$url = $FullURL->toString();
				$uquery =&JURI::getInstance($url);
				$ExtendedQuery = $uquery->getQuery();
				if (strlen($ExtendedQuery)>0) { $url = $uquery->getPath() ."?" .$uquery->getQuery(); }
				else { $url = $uquery->getPath(); }*/
				$phpURL = http_build_query($_GET);
				$FullURL =& JFactory::getURI();
				$url = $FullURL->toString();
				$uquery =&JURI::getInstance($url);
				$ExtendedQuery = $uquery->getQuery();
				if (strlen($ExtendedQuery)>0) { $url = $uquery->getPath() ."?" .$uquery->getQuery(); }
				else { $url = $uquery->getPath(); }
				
				//DB Instance
				$db =& JFactory::getDBO();
				//Submit SEO information if post was made
				$seo_post = JRequest::getVar('seo_post');
				if ($seo_post == 1) {
					$p_seo_title = addslashes(JRequest::getVar('seo_title'));
					$p_seo_keywords = addslashes(JRequest::getVar('seo_keywords'));
					$p_seo_description = addslashes(JRequest::getVar('seo_description'));
					$p_seo_update = JRequest::getVar('seo_update');
					//check if url already exists in db
					$query = "SELECT * FROM #__easyseo WHERE url = '$phpURL'";
					$db->setQuery($query);
					$urlindb = $db->loadRow();
					if (count($urlindb)>0) { $p_seo_update = 1;}
					if ($p_seo_update != 1) {
						$query = "INSERT INTO #__easyseo (url,title,keywords,description) VALUES('$phpURL','$p_seo_title','$p_seo_keywords','$p_seo_description')";
					}
					else {
						$query = "UPDATE #__easyseo SET title='$p_seo_title', keywords='$p_seo_keywords', description='$p_seo_description' WHERE url='$phpURL'";
					}
					$db->setQuery($query);
					if (!$db->query()) { echo $db->ErrorMsg() ."<br/>"; } 
				
				}
				
				//Retrive DB Information
				$doc =& JFactory::getDocument();
				$query = "SELECT keywords, description, title FROM #__easyseo where url='$phpURL'";
				$db->setQuery($query);
				$data = $db->LoadRow();
						$keywords = $data[0];
						$description = $data[1];
						$title = $data[2];
						if (strlen($title) == 0) { global $mainframe; $title = $doc->getTitle();}
				//mainframe
				$mainframe =& JFactory::getApplication('site'); //instance mainframe
				if ($keywords != "") { $doc->setMetaData('keywords',$keywords); } //set the Keywords Metatag
				if ($description != "") { $doc->setMetaData('description',$description); } //set the Description Metatag
				if ($title != "") {$doc->setMetaData('title',$title); }
				$doc->setTitle($title); //set page title
				
				
				
				//FRONT END GUI
				$doc =& JFactory::getDocument();
				$user =& JFactory::getUser();
				$userid = $user->id;
				$AdminCatID = $this->params->get('access_level');

				$query = "SELECT group_id FROM #__user_usergroup_map WHERE user_id = $userid";
				$db->setQuery($query);
				$DBAdminCatID = $db->loadResult();
					if ($AdminCatID == $DBAdminCatID) {
						$doc->addStyleSheet("plugins/system/smartseo/easyseo/style.css");
						$doc->addScript("plugins/system/smartseo/easyseo/toggleid.js");
					}
					
				
			 } 
		 }
}



?>