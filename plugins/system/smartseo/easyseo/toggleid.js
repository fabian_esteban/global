function visibility_smart(id) {
var e = document.getElementById(id);
if(e.style.display == 'block')
e.style.display = 'none';
else
e.style.display = 'block';
}

function metaTitle() {
metaCollection = document.getElementsByTagName('meta');
	for (i=0;i<metaCollection.length;i++) {
	nameAttribute = metaCollection[i].name.search(/title/);
	
		if (nameAttribute!= -1) {
			document.forms['easyseo_form'].seo_title.value = metaCollection[i].content;
		}
	}
} 


function metaKeywords() {
metaCollection = document.getElementsByTagName('meta');
	for (i=0;i<metaCollection.length;i++) {
	nameAttribute = metaCollection[i].name.search(/keywords/);
	
		if (nameAttribute!= -1) {
			document.forms['easyseo_form'].seo_keywords.value = metaCollection[i].content;
		}
	}
} 

function metaDescription() {
metaCollection = document.getElementsByTagName('meta');
	for (i=0;i<metaCollection.length;i++) {
	nameAttribute = metaCollection[i].name.search(/description/);
	
		if (nameAttribute!= -1) {
			document.forms['easyseo_form'].seo_description.value = metaCollection[i].content;
		}
	}
} 

<!-- Dynamic Version by: Nannette Thacker -->
<!-- http://www.shiningstar.net -->
<!-- Original by :  Ronnie T. Moore -->
<!-- Web Site:  The JavaScript Source -->
<!-- Use one function for multiple text areas on a page -->
<!-- Limit the number of characters per textarea -->
<!-- Modified by JJExtensions for Smart SEO -->
<!-- Begin
function textCounter(field,cntfield,maxlimit) {
	cntfield.value = field.value.length;
}
//  End -->