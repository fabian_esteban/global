<?php

/*********************************************************************************
* LiveZilla groups.inc.php
* 
* Copyright 2011 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes in this file may cause critical errors.
* To modify LiveZilla Server settings it is strongly recommended to use 
* LiveZilla Server Admin application and desist from editing this file directly.
* 
********************************************************************************/ 

$GROUPS = Array();
$GROUPS["Soporte"] = Array();
$GROUPS["Soporte"]["gr_desc"] = "YToxOntzOjI6IkVOIjtzOjEyOiJVMjl3YjNKMFpRPT0iO30=";
$GROUPS["Soporte"]["gr_external"] = 1;
$GROUPS["Soporte"]["gr_internal"] = 1;
$GROUPS["Soporte"]["gr_created"] = "1415375495";
$GROUPS["Soporte"]["gr_email"] = "soporte@globalcdb.com";
$GROUPS["Soporte"]["gr_standard"] = 1;
$GROUPS["Soporte"]["gr_vfilters"] = "YTowOnt9";
$GROUPS["Soporte"]["gr_hours"] = array();
$GROUPS["Soporte"]["gr_ex_sm"] = "1";
$GROUPS["Soporte"]["gr_ex_so"] = "1";
$GROUPS["Soporte"]["gr_ex_pr"] = "1";
$GROUPS["Soporte"]["gr_ex_ra"] = "1";
$GROUPS["Soporte"]["gr_ex_fv"] = "0";
$GROUPS["Soporte"]["gr_ex_fu"] = "1";
$GROUPS["Soporte"]["gr_ci_hidden"] = Array();
$GROUPS["Soporte"]["gr_ti_hidden"] = Array();
$GROUPS["Soporte"]["gr_ci_mand"] = Array();
$GROUPS["Soporte"]["gr_ti_mand"] = Array();
?>