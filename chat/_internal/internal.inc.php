<?php

/*********************************************************************************
* LiveZilla internal.inc.php
* 
* Copyright 2011 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes in this file may cause critical errors.
* To modify LiveZilla Server settings it is strongly recommended to use 
* LiveZilla Server Admin application and desist from editing this file directly.
* 
********************************************************************************/ 

$INTERN = Array();
$INTERN["6da97b5"] = Array();
$INTERN["6da97b5"]["in_id"] = "administrator";
$INTERN["6da97b5"]["in_level"] = "1";
$INTERN["6da97b5"]["in_groups"] = "YToxOntpOjA7czoxMjoiVTI5d2IzSjBaUT09Ijt9";
$INTERN["6da97b5"]["in_name"] = "Soporte";
$INTERN["6da97b5"]["in_desc"] = "";
$INTERN["6da97b5"]["in_email"] = "info@globalcdb.com";
$INTERN["6da97b5"]["in_websp"] = 5;
$INTERN["6da97b5"]["in_perms"] = "2222222";
$INTERN["6da97b5"]["in_lang"] = "ES";
$INTERN["6da97b5"]["in_aac"] = "1";
$INTERN["6da97b5"]["in_lipr"] = "";
?>