<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.application.component.controller' );


// Begining of the controller
class UsuariosControllerUsuarios extends JController{


	public function saveJuridica(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    $data = $userModel->getUser( $usuario['email'] );

	    if ( !empty ($data) ) {
	    	$response->status = 300;
	    	$response->message = 'El usuario '.$usuario['email'].' ya existe.';
	    	echo json_encode($response);
			die;
	    }

		//generate password
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < 32; $i++) {
	        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
	    }

	    // generates joomla password
	    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );

	    $args = array(
	    		'name' => $usuario['nombre']
	    	,	'username' => $usuario['identificador']
	    	,	'email' => $usuario['email']
	    	,	'password' => $usuario['password']
	    	,	'registerDate' => $date
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'web' => $usuario['web']
	    	,	'block' => '1'
	    	,	'tipo' =>  '2'
	    	,	'cantidad_dias' => 1
	    );

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

	    $id = $userModel->insertId;

	    // merge group registered with user
	    $groupArgs = array('user_id' => $id , 'group_id' => '2' );
	    $groupModel = $this->getModel('groups');
	    $groupModel->instance( $groupArgs );
	    $groupModel->saveGroup();

	    $this->sendMailAdmin( $usuario, 'juridica' );

	    $response->status = 200;
	    $response->message = 'Registro Creado correctamente';
		echo json_encode($response);
		die;

	}	

	public function saveInversionista(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    $data = $userModel->getUser( $usuario['email'] );

	    if ( !empty ($data) ) {
	    	$response->status = 300;
	    	$response->message = 'El usuario '.$usuario['email'].' ya existe.';
	    	echo json_encode($response);
			die;
	    }

		//generate password
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < 32; $i++) {
	        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
	    }

	    // generates joomla password
	    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );

	    $args = array(
	    		'name' => $usuario['nombre']
	    	,	'username' => $usuario['identificador']
	    	,	'email' => $usuario['email']
	    	,	'password' => $usuario['password']
	    	,	'registerDate' => $date
	    	,	'fecha_nacimiento' => $usuario['fecha_nacimiento']
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'celular' =>  $usuario['celular']
	    	,	'apellidos' => $usuario['apellidos']
	    	,	'tipo' =>  '1'
	    	,	'block' => '1'
	    	,	'cantidad_dias' => 1
	    );

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

	    $id = $userModel->insertId;

	    // merge group registered with user
	    $groupArgs = array('user_id' => $id , 'group_id' => '2' );
	    $groupModel = $this->getModel('groups');
	    $groupModel->instance( $groupArgs );
	    $groupModel->saveGroup();

	    $this->sendMailAdmin( $usuarios, 'inversionista' );

	    $response->status = 200;
	    $response->message = 'Registro Creado correctamente';
		echo json_encode($response);
		die;

	}

	public function sendMailAdmin( $usuario, $tipo ){

		$mail = JFactory::getMailer();

		$date = date( 'Y-m-d H:i:s' );

		$contenido = "<div><img src='".JURI::root()."images/cabezote.jpg'><br><br>";
		$contenido .= "Cordial saludo,<br>";
		$contenido .= "<b>Administrador</b><br><br>";
		$contenido .= "Un nuevo usuario se ha registrado en la plataforma, a continuación encontrará los datos de dicho usuario:<br><br>";

		if ($tipo == 'inversionista') {
			$contenido .= "<b>Tipo usuario:</b> Inversionista<br>";
			$contenido .= "<b>Nombre y apellido:</b> ".$usuario['name']." ".$usuario['apellidos']."<br>";
			$contenido .= "<b>Email:</b> ".$usuario['email']."<br>";
			$contenido .= "<b>Cédula:</b> ".$usuario['identificador']."<br>";
			$contenido .= "<b>Fecha de registro:</b> ".Misc::formatDateSpanish($date)."<br>";
			$contenido .= "<b>Fecha de nacimiento:</b> ".$usuario['fecha_nacimiento']."<br>";
			$contenido .= "<b>Dirección:</b> ".$usuario['direccion']."<br>";
			$contenido .= "<b>Ciudad:</b> ".$usuario['ciudad']."<br>";
			$contenido .= "<b>Teléfono:</b> ".$usuario['telefono']."<br>";
			$contenido .= "<b>Celular:</b> ".$usuario['celular']."<br>";
		}

		if ($tipo == 'juridica') {
			$contenido .= "<b>Tipo usuario:</b> Juridica<br>";
			$contenido .= "<b>Nombre de la compañía:</b> ".$usuario['name']."<br>";
			$contenido .= "<b>Email:</b> ".$usuario['email']."<br>";
			$contenido .= "<b>NIT:</b> ".$usuario['identificador']."<br>";
			$contenido .= "<b>Fecha de registro:</b> ".Misc::formatDateSpanish($date)."<br>";
			$contenido .= "<b>Dirección:</b> ".$usuario['direccion']."<br>";
			$contenido .= "<b>Ciudad:</b> ".$usuario['ciudad']."<br>";
			$contenido .= "<b>Teléfono:</b> ".$usuario['telefono']."<br>";
			$contenido .= "<b>Página Web:</b> ".$usuario['web']."<br><br>";
		}


		$contenido .= "Recuerde activar el usuario dentro de la plataforma.<br><br>";
		$contenido .= "Muchas gracias.<br><br>";
		$contenido .= "Cordialmente,<br><br>";
		$contenido .= "<b>Global Securities Colombia.</b><br><br>";
		$contenido .= "<img src='".JURI::root()."images/pie.jpg'></div>";

		$mail->setSender( 'contactenos@globalcdb.com' );
		$mail->addRecipient( 'contactenos@globalcdb.com, alejandro.suarez@sainetingenieria.com' );
		$mail->setSubject( "Un nuevo usuario está registrado y pendiente de activación" );
		$mail->Encoding = 'base64';
		$mail->isHtml( true );
		$mail->setBody( $contenido );

        // enviar email
        $mail->Send();
	}

	public function editarJuridica(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );


	    $args = array(
	    		'id' => $usuario['id']
	    	,	'name' => $usuario['nombre']
	    	,	'username' => $usuario['identificador']
	    	,	'email' => $usuario['email']
	    	,	'fecha_edicion' => $date
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'web' => $usuario['web']
	    );

	    if( ! empty( $usuario[ 'password' ] ) ) {
			
			//generate password
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		    for ($i = 0; $i < 32; $i++) {
		        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
		    }

		    // generates joomla password
		    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

		    $pass = array('password' => $usuario[ 'password' ], 'cantidad_dias' => 1, 'fecha_edicion_pass' => $date );

		    $args = array_merge( $args, $pass );

		    
	    }

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

	    $this->sendMailEdit( $usuario, 'juridica' );

		$response->status = 200;
	    $response->message = 'Usuario editado correctamente.';
		echo json_encode($response);
		die;

	}

	public function editarInversionista(){

		$response = new stdClass();

		$usuario = JRequest::getVar( 'usuario' );

		//get model of user
	    $userModel = $this->getModel('usuarios');

	    // date of register
	    $date = date( 'Y-m-d H:i:s' );


	    $args = array(
	    		'id' => $usuario['id']
	    	,	'name' => $usuario['nombre']
	    	,	'username' => $usuario['identificador']
	    	,	'email' => $usuario['email']
	    	,	'fecha_edicion' => $date
	    	,	'fecha_nacimiento' => $usuario['fecha_nacimiento']
	    	,	'direccion' => $usuario['direccion']
	    	,	'ciudad' => $usuario['ciudad']
	    	,	'telefono' =>  $usuario['telefono']
	    	,	'celular' =>  $usuario['celular']
	    	,	'apellidos' => $usuario['apellidos']
	    );



	    if( ! empty( $usuario[ 'password' ] ) ) {
			
			//generate password
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		    for ($i = 0; $i < 32; $i++) {
		        $randomString .= $characters[ rand(0, strlen( $characters ) - 1)];
		    }

		    // generates joomla password
		    $usuario[ 'password' ] = md5( $usuario[ 'password' ] . $randomString ) . ':' . $randomString;

		    $pass = array('password' => $usuario[ 'password' ], 'cantidad_dias' => 1, 'fecha_edicion_pass' => $date  );

		    $args = array_merge( $args, $pass );
	    }

	    // instance model
	    $userModel->instance( $args );


	    if( ! $userModel->save('bool') ){
	    	$response->status = 500;
	    	$response->message = 'No se pudo realizar el registro. Intente más tarde';
	    	echo json_encode($response);
			die;
	    }

	    $this->sendMailEdit( $usuario, 'inversionista' );

		$response->status = 200;
	    $response->message = 'Usuario editado correctamente.';
		echo json_encode($response);
		die;

	}

	public function sendMailEdit( $usuario, $tipo ){

		$mail = JFactory::getMailer();

		$date = date( 'Y-m-d H:i:s' );

		$contenido = "<div><img src='".JURI::root()."images/cabezote.jpg'><br><br>";
		$contenido .= "Cordial saludo,<br>";
		$contenido .= "<b>Administrador</b><br><br>";
		$contenido .= "Uno de los usuarios ha editado su cuenta dentro de la plataforma, a continuación encontrará los datos del usuario:<br><br>";

		if ($tipo == 'inversionista') {
			$contenido .= "<b>Tipo usuario:</b> Inversionista<br>";
			$contenido .= "<b>Nombre y apellido:</b> ".$usuario['name']." ".$usuario['apellidos']."<br>";
			$contenido .= "<b>Email:</b> ".$usuario['email']."<br>";
			$contenido .= "<b>Cédula:</b> ".$usuario['identificador']."<br>";
			$contenido .= "<b>Fecha de modificación:</b> ".Misc::formatDateSpanish($date)."<br>";
			$contenido .= "<b>Fecha de nacimiento:</b> ".$usuario['fecha_nacimiento']."<br>";
			$contenido .= "<b>Dirección:</b> ".$usuario['direccion']."<br>";
			$contenido .= "<b>Ciudad:</b> ".$usuario['ciudad']."<br>";
			$contenido .= "<b>Teléfono:</b> ".$usuario['telefono']."<br>";
			$contenido .= "<b>Celular:</b> ".$usuario['celular']."<br><br>";
		}

		if ($tipo == 'juridica') {
			$contenido .= "<b>Tipo usuario:</b> Juridica<br>";
			$contenido .= "<b>Nombre de la compañía:</b> ".$usuario['name']."<br>";
			$contenido .= "<b>Email:</b> ".$usuario['email']."<br>";
			$contenido .= "<b>NIT:</b> ".$usuario['identificador']."<br>";
			$contenido .= "<b>Fecha de registro:</b> ".Misc::formatDateSpanish($date)."<br>";
			$contenido .= "<b>Dirección:</b> ".$usuario['direccion']."<br>";
			$contenido .= "<b>Ciudad:</b> ".$usuario['ciudad']."<br>";
			$contenido .= "<b>Teléfono:</b> ".$usuario['telefono']."<br>";
			$contenido .= "<b>Página Web:</b> ".$usuario['web']."<br><br>";
		}
	
		$contenido .= "Muchas gracias.<br><br>";
		$contenido .= "Cordialmente,<br><br>";
		$contenido .= "<b>Global Securities Colombia.</b><br><br>";
		$contenido .= "<img src='".JURI::root()."images/pie.jpg'></div>";

		

		$mail->setSender( 'contactenos@globalcdb.com' );
		$mail->addRecipient( 'alejandro.suarez@sainetingenieria.com' );
		$mail->setSubject( "Un usuario ha editado su cuenta" );
		$mail->Encoding = 'base64';
		$mail->isHtml( true );
		$mail->setBody( $contenido );

        // enviar email
        $mail->Send();
	}	

	public function setRegisterDays(){

		$date = date('Y-m-d');

		$fechaEdicion = strtotime ( '-180 day' , strtotime ( $date ) ) ;

		$fechaEdicion = date ( 'Y-m-d' , $fechaEdicion );

		$userModel = $this->getModel( 'usuarios' );

		$users = $userModel->getUsersBlock( $fechaEdicion );

		foreach ($users as $key => $usuario) {
			$args = array(
					'id' => $usuario->id
				,	'block' => '1'
			);

			$userModel->instance( $args );

			$userModel->save();
		}

		

	}

	public function sendMailAdviceUser(){

		$userModel = $this->getModel( 'usuarios' );

		$usuarios = $userModel->getObjects();

		foreach ($usuarios as $key => $usuario) {

			$fechaEdicion = strtotime ( '+165 day' , strtotime ( $usuario->fecha_edicion_pass ) ) ;

			$fechaEdicion = date ( 'Y-m-d H:i:s' , $fechaEdicion );

			$segundos= strtotime($fechaEdicion) - strtotime('now');

			$diferencia_dias=intval($segundos/60/60/24);

			$diferencia_dias = $diferencia_dias * (-1);

			if ($diferencia_dias <= 15) {

				$mail = JFactory::getMailer();

				$contenido = "<div><img src='".JURI::root()."images/cabezote.jpg'><br><br>";
				$contenido .= "Cordial saludo,<br>";
				$contenido .= "<b>Sr. (a) ".$usuario->name."</b><br><br>";
				$contenido .= "Cordialmente le informo que debe cambiar su contraseña antes de los próximos ".$diferencia_dias." días. Recuerde que si usted no cambia la contraseña en este plazo su usuario será desactivado automáticamente.<br><br>";
				$contenido .= "Cualquier duda no dude en comunicarse con nosotros<br><br>";
				$contenido .= "Muchas gracias.<br><br>";
				$contenido .= "Cordialmente,<br><br>";
				$contenido .= "<b>Global Securities Colombia.</b><br><br>";
				$contenido .= "<img src='".JURI::root()."images/pie.jpg'></div>";

				$mail->setSender( 'contactenos@globalcdb.com' );
				$mail->addRecipient( $usuario->email );
				$mail->setSubject( "Advertencia cambio contraseña - Global Securities Colombia" );
				$mail->Encoding = 'base64';
				$mail->isHtml( true );
				$mail->setBody( $contenido );

		        // enviar email
		        $mail->Send();

	        }		
		}

	}
	
}
?>