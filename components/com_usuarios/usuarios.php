
<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
require_once( JPATH_COMPONENT . DS . 'controller.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/misc.php' );
require_once( JPATH_COMPONENT . DS . 'helpers/helper.php' );



$controller = JControllerLegacy::getInstance('Usuarios');
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();

?>