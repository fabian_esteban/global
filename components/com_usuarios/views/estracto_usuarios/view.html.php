<?php

/**
 * General View for "informes seguimiento" "lista" layout
 * 
 */

// Joomla calls and runtimes
defined( '_JEXEC' ) or die();
jimport( 'joomla.application.component.view' );

class UsuariosViewEstracto_Usuarios extends JView {

	public $usuario;
	// Function that initializes the view
	function display( $tpl = null ){

		$this->usuario = $this->get( 'Usuarios' );

		$app = JFactory::getApplication();
		
		parent::display( $tpl );
	}

}
?>