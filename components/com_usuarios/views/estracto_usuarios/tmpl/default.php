<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

$user = JFactory::getUser();
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root
$app = JFactory::getApplication();

if ( $user->guest ){
	$app->redirect($url.'index.php/tipo-de-registro','Por favor inicie sesión', 'error');
} 
	
$userLog = UsuariosHelper::getLogUser();

$fechaEdicion = strtotime ( '+165 day' , strtotime ( $user->fecha_edicion_pass ) ) ;
$fechaEdicion = date ( 'Y-m-d H:i:s' , $fechaEdicion );
$segundos= strtotime($fechaEdicion) - strtotime('now');
$diferencia_dias=intval($segundos/60/60/24);
$diferencia_dias = $diferencia_dias * (-1);

$meses = array(
		'1' => 'Enero'
	, 	'2' => 'Febrero'
	, 	'3' => 'Marzo'
	, 	'4' => 'Abril'
	, 	'5' => 'Mayo'
	, 	'6' => 'Junio'
	, 	'7' => 'Julio'
	, 	'8' => 'Agosto'
	, 	'9' => 'Septiembre'
	, 	'10' => 'Octubre'
	, 	'11' => 'Noviembre'
	, 	'12' => 'Diciembre'	
);

?>


<div class="logeo-link">
	
	<span class="ingreso"> Ultimo ingreso: <?= Misc::formatDateSpanish($userLog->date) ?></span>
	<span class="ingreso"> Desde: <?= $userLog->ip ?></span>
	<a href="index.php/usuarios" class="my-account">Mi cuenta</a>
	
</div>

<?php 
if ($diferencia_dias <= 15): 

?>
	<div class="vencimiento">
		<a href="index.php/usuarios"> Faltan <?= $diferencia_dias ?> días para el vencimiento de su cuenta</a>
	</div>
<?php 
endif 
?>

<div class="content-extractos">
	<div class="last-extractos">
		<h3>ÚLTIMOS EXTRACTOS</h3>

		<table class="list-extractos">
			<tr>
				<td>Extracto Noviembre 2014</td>
				<td><a href="index.php/extracto-usuario?layout=detail">Ver más</a></td>
			</tr>
			<tr>
				<td>Extracto Octubre 2014</td>
				<td><a href="index.php/extracto-usuario?layout=detail">Ver más</a></td>
			</tr>
			<tr>
				<td>Extracto Septiembre 2014</td>
				<td><a href="index.php/extracto-usuario?layout=detail">Ver más</a></td>
			</tr>
		</table>
	</div>

	<div class="historial-extractos">
		<h3>HISTORIAL DE EXTRACTOS</h3>


		<div class="content-historial">
			<span>Seleccione el mes y el año del extracto que desea consultar</span>

			<ul>
				<li>
					<label>Mes</label>
				</li>
				<li>
					<select class="mes" name="mes">
						<?php foreach ($meses as $key => $mes) {
						?>
						<option value="<?= $key ?>"><?= $mes ?></option>
						<?php
						} ?>

					</select>
				</li>

				<li>
					<label>Año</label>
				</li>
				<li>
					<select class="year" name="year">


						<?php

						$already_selected_value = 2014;
						$earliest_year = 2000;

						foreach (range(date('Y'), $earliest_year) as $x) {
						    print '<option value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';
						}
						
						?>

					</select>
				</li>
			</ul>

			<a class="find-extract" href="#">Buscar</a>
		</div>
	</div>
</div>