<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

$user = JFactory::getUser();
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root
$app = JFactory::getApplication();

if ( $user->guest ){
	$app->redirect($url.'index.php/tipo-de-registro','Por favor inicie sesión', 'error');
} 
	
$userLog = UsuariosHelper::getLogUser();
?>

<div class="logeo-link">
	
	<span class="ingreso"> Ultimo ingreso: <?= Misc::formatDateSpanish($userLog->date) ?></span>
	<span class="ingreso"> Desde: <?= $userLog->ip ?></span>
	<a href="index.php/usuarios" class="my-account">Mi cuenta</a>
	
</div>

<?php 

$fechaEdicion = strtotime ( '+165 day' , strtotime ( $user->fecha_edicion_pass ) ) ;

$fechaEdicion = date ( 'Y-m-d H:i:s' , $fechaEdicion );

$segundos= strtotime($fechaEdicion) - strtotime('now');

$diferencia_dias=intval($segundos/60/60/24);

$diferencia_dias = $diferencia_dias * (-1);

?>
<?php 
if ($diferencia_dias <= 15): 

?>
	<div class="vencimiento">
		<a href="index.php/usuarios"> Faltan <?= $diferencia_dias ?> días para el vencimiento de su cuenta</a>
	</div>
<?php endif ?>

<div class="extracto-top">
		<ul>
			<li> <strong> Nombre Cliente: </strong> SANCHES HERRERA NATALIA </li>
			<li> <strong> Dirección: </strong> CARRERA 771 - 21 TORRE A OF 601 </li>
			<li> <strong> Ciudad: </strong> BOGOTÁ</li>
			<li> <strong> Asesor Comercial: </strong> EDUARDO ANDRADE JIMENO </li>
			<li> <strong> Estado: </strong> ACTIVO</li>
			<li> <strong> Informe del: </strong> 26 - 11 - 2014</li>
		</ul>
</div>

<div class="extracto-usuario">

	<table class="info_detalle" border="1" width="100%">
		<tbody>
			<tr><th>INFORMACIÓN DETALLADA</th>
			<th>COMPOSICIÓN DEL PORTAFOLIO</th>
			</tr>
			<tr>
			<td>Renta variable:  <span class="variable"></span></td>
			<td rowspan="9" class="td_diagrama"> <div id="diagrama_charts"> </div> </td>
			</tr>
			<tr>
			<td>Renta Fija:  <span class="fija"></span></td>
			</tr>
			<tr>
			<td>Operaciones de liquidez:  <span class="o_liquidez"></span></td>
			</tr>
			<tr>
			<td>Saldo disponible: <span class="s_disponible"></span></td>
			</tr>
			<tr>
			<td class="resalta">TOTAL  <span class="total"> 1 </span>  </td>
			</tr>
			<tr>
			<td>CUENTA DE MARGEN <span class="c_margen"></span></td>
			</tr>
			<tr>
			<td>Garantiá  <span class="garantia"></span></td>
			</tr>
			<tr>
			<td>CARTERAS COLECTIVAS  <span class="c_colectivas">11.683.289</span></td>
			</tr>
			<tr>
			<td class="resalta" >GRAN TOTAL <span class="g_total">11.683.289</span></td>
			</tr>
		</tbody>
	</table>

	<table class="cartera" width="100%">
		<tbody>
			<tr>
				<th colspan="5">CARTERAS COLECTIVAS</th>
			</tr>
			<tr class="titulos" >
				<td colspan="5">CCE GS CREDIT OPORTUNITIES FUND COMPORTAMIENTO TITULOS VALORES</td>
			</tr>
			<tr class="subtitulos">
				<td>Encargo</td>
				<td>V. de la Unidad</td>
				<td>F. Contitución</td>
				<td>Saldo en pesos</td>
				<td>N. Unidades</td>
			</tr>
			<tr class="valores">
				<td> 5454 </td>
				<td> 12.653 </td>
				<td>12/05/2010</td>
				<td>9.437.000</td>
				<td>747</td>
			</tr>
			<tr class="titulos" >
				<td colspan="5">CCA GLOBAL SECIRITIES ACCIONES COMPORTAMIENTO ACCIONES</td>
			</tr>
			<tr class="subtitulos">
				<td>Encargo</td>
				<td>V. de la Unidad</td>
				<td>F. Contitución</td>
				<td>Saldo en pesos</td>
				<td>N. Unidades</td>
			</tr>
			<tr class="valores">
				<td> 5454 </td>
				<td> 12.653 </td>
				<td>12/05/2010</td>
				<td>9.437.000</td>
				<td>747</td>
			</tr>
			<tr class="titulos" >
				<td colspan="5">CARTERA COLECTIVA ABIERTA “GLOBAL VISTA”</td>
			</tr>
			<tr class="subtitulos">
				<td>Encargo</td>
				<td>V. de la Unidad</td>
				<td>F. Contitución</td>
				<td>Saldo en pesos</td>
				<td>N. Unidades</td>
			</tr>
			<tr class="valores">
				<td> 5454 </td>
				<td> 12.653 </td>
				<td>12/05/2010</td>
				<td>9.437.000</td>
				<td>747</td>
			</tr>
		</tbody>
	</table>

	<table class="extract-pie">
		<tr>
			<td>	
				<img src="components/com_usuarios/assets/images/LogoSuperIntendencia.jpg" width="170">
			</td>
			<td class="text-right">
			<span class="title-pie">Defensor Del Consumidor Finaciero</span>
Cualquier incorformidad con la información presentada agradecemos comunicarla a la revisoría fiscal: CARRERA 6 No . 14 76 oficina 1205 - Teléfonos (1) 2823570 - Celular: 313 364 4105 E-mail: ptsilvadefensor@hotmail.com, defensordelcliente@globalcdb.com
			</td>

		</tr>

		<tr>
			<td class="pie-complement" colspan="2">			
				Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
			</td>

		</tr>			
	</table>

</div>
