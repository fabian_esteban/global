<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

$app = JFactory::getApplication();

if ($this->usuario->tipo == '2') {
	$app->redirect( 'index.php/component/usuarios/?layout=editar_usuario' );
}	

?>

<h2> <?php echo JText::_('INVERSIONISTAS_INDIVIDUALES'); ?></h2>

<div id="componente-extractos">

	<form id="reg-inversionistas-form">
	<table class="registros" border="0" >
		<tbody>
		<tr>
			<td colspan="2"> <label> <?php echo JText::_('NOMBRE_USUARIO'); ?>  </label> <input type="text" name="nombre" ></td>
		</tr>
		<tr>
			<td><label> <?php echo JText::_('APELLIDOS_USUARIO'); ?>  </label> <input type="text" name="apellidos" ></td>

			<td><label> <?php echo JText::_('EMAIL_USUARIO'); ?>  </label> <input type="text" name="email" ></td>
		</tr>
		<tr>
			<td> <label> <?php echo JText::_('NUMERO_CEDULA_USUARIO'); ?>  </label> <input type="text" name="identificador" ></td>
			
			<td> <label> <?php echo JText::_('NACIMIENTO_USUARIO'); ?>  </label> <input type="text" id="fecha" name="fecha_nacimiento" ></td>
		</tr>
		<tr>
			<td> <label> <?php echo JText::_('CONTRASENIA_USUARIO'); ?>  </label> <input type="password" name="password" ></td>			
			
			<td> <label> <?php echo JText::_('CONFIRMAR_CONTRASENIA_USUARIO'); ?>  </label> <input type="password" name="confirmar_password" ></td>
		</tr>
		<tr>
			<td> <label> <?php echo JText::_('TELEFONO_USUARIO'); ?>  </label> <input type="text" name="telefono" ></td>
		
			<td> <label> <?php echo JText::_('CELULAR_USUARIO'); ?>  </label> <input type="text" name="celular" ></td>
		</tr>
		<tr>
			<td> <label> <?php echo JText::_('DIRECCION_USUARIO'); ?>  </label> <input type="text" name="direccion" ></td>
		
			<td> <label> <?php echo JText::_('CIUDAD_USUARIO'); ?>  </label> <input type="text" name="ciudad" ></td>		
		</tr>
		<tr>
			<td colspan="2"> <input type="checkbox" name="terminos" id="terminos" > <span class="t_condicioneds" > 
				Autorizo a Global Securities para que me envíe información de distinta índole a mi correo electrónico (movimientos, extractos, análisis financieros, información de interés a mis inversiones, entre otros).
				Por su seguridad, la información registrada será corroborada con la documentación de su apertura de cuenta. </span>  
			</td>
		</tr>
		<tr>
			<td colspan="2"> 
				<input type="submit" value="Enviar" name="enviar-inv" id="enviar-inv" class="enviar" >
				<input type="reset" value="Borrar" name="reset" class="reset">
			 </td>
		</tr>
		</tbody>
	</table>
	</form>
</div>