<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

$app = JFactory::getApplication();

if ($this->usuario->tipo == '1') {
	$app->redirect( 'index.php/usuarios/?layout=editar_usuario' );
}	


?>
<h2> <?php echo JText::_('Editar Cuenta'); ?></h2>

<div id="componente-extractos">

	<form id="edit-juridicas-form">
		<table class="registros" border="0" >
			<tbody>
			<tr>
				<input type="hidden" value="<?= $this->usuario->id ?>" name="id">
				<td><label> <?php echo JText::_('NOMBRE_COMPANIA'); ?>  </label> <input type="text" name="nombre" value="<?= $this->usuario->name ?>"></td>
			</tr>
			<tr>
				<td><label> <?php echo JText::_('NIT_COMPANIA'); ?>  </label> <input type="text" name="identificador" value="<?= $this->usuario->username ?>" disabled ></td>
				<td><label> <?php echo JText::_('EMAIL_COMPANIA'); ?>  </label> <input type="text" name="email" value="<?= $this->usuario->email ?>" ></td>
			</tr>	
			<tr>
				<td><label> <?php echo JText::_('CONTRASENIA_USUARIO'); ?>  </label> <input type="password" name="password" ></td>
				<td><label> <?php echo JText::_('CONFIRMAR_CONTRASENIA_USUARIO'); ?>  </label> <input type="text" name="confirmar_password"  ></td>				
			</tr>

			<tr>
				<td><label> <?php echo JText::_('TELEFONO_USUARIO'); ?>  </label> <input type="text" name="telefono" value="<?= $this->usuario->telefono ?>" ></td>
				<td><label> <?php echo JText::_('DIRECCION_USUARIO'); ?>  </label> <input type="text" name="direccion" value="<?= $this->usuario->direccion ?>" ></td>				
			</tr>

			<tr>
				<td><label> <?php echo JText::_('CIUDAD_USUARIO'); ?>  </label> <input type="text" name="ciudad" value="<?= $this->usuario->ciudad ?>" ></td>
				<td><label> <?php echo JText::_('PAGINA_WEB'); ?>  </label> <input type="text" name="web" value="<?= $this->usuario->web ?>" ></td>
			</tr>
	
			<tr>
				<td colspan="2"> 
					<input type="submit" value="Editar" name="editar-jur" id="editar-jur" class="enviar" >
					<input type="reset" value="Borrar" name="reset" class="reset">
				 </td>
			</tr>	
			</tbody>
		</table>
	</form>
</div>