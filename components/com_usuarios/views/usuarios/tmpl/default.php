<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');


$user = JFactory::getUser();
$uri = &JURI::getInstance(); // base url object
$url = $uri->root(); // url root
$app = JFactory::getApplication();


if (! $user->guest ){

	if ($this->usuario->tipo == "2"){
		$app->redirect( 'index.php/usuarios/?layout=editar_usuario_j', '', false );
	}
	else{
		$app->redirect( 'index.php/usuarios/?layout=editar_usuario', '', false);
	}
	
} else{
	$app->redirect($url.'index.php/tipo-de-registro','Por favor inicie sesión', 'error');
}
?>








