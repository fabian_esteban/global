<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

$app = JFactory::getApplication();

if ($this->usuario->tipo == '2') {
	$app->redirect( 'index.php/component/usuarios/?layout=editar_usuario_j' );
}

?>

<h2> <?php echo JText::_('Editar Cuenta'); ?></h2>

<div id="componente-extractos">

	<form id="edit-inversionistas-form">
		<table class="registros" border="0" >
			<tbody>
			<tr>
				<input type="hidden" value="<?= $this->usuario->id ?>" name="id">
				<td colspan="4"> <label> <?php echo JText::_('NOMBRE_USUARIO'); ?>  </label> <input type="text" name="nombre" value="<?= $this->usuario->name ?>" ></td>
			</tr>
			<tr>
				<td><label> <?php echo JText::_('APELLIDOS_USUARIO'); ?>  </label> <input type="text" name="apellidos" value="<?= $this->usuario->apellidos ?>"></td>

				<td><label> <?php echo JText::_('EMAIL_USUARIO'); ?>  </label> <input type="text" name="email" value="<?= $this->usuario->email ?>"></td>
			</tr>
			<tr>
				<td> <label> <?php echo JText::_('NUMERO_CEDULA_USUARIO'); ?>  </label> <input type="text" name="identificador" value="<?= $this->usuario->username ?>" readonly></td>

				<td> <label> <?php echo JText::_('NACIMIENTO_USUARIO'); ?>  </label> <input type="text" name="fecha_nacimiento" id="fecha" value="<?= $this->usuario->fecha_nacimiento ?>"></td>
			</tr>
			<tr>
				<td> <label> <?php echo JText::_('CONTRASENIA_USUARIO'); ?>  </label> <input type="password" name="password" ></td>
				
				<td> <label> <?php echo JText::_('CONFIRMAR_CONTRASENIA_USUARIO'); ?>  </label> <input type="password" name="confirmar_password" ></td>
			</tr>
			<tr>
				<td> <label> <?php echo JText::_('TELEFONO_USUARIO'); ?>  </label> <input type="text" name="telefono" value="<?= $this->usuario->telefono ?>"></td>
			
				<td> <label> <?php echo JText::_('CELULAR_USUARIO'); ?>  </label> <input type="text" name="celular" value="<?= $this->usuario->celular ?>"></td>
			</tr>
			<tr>
				<td> <label> <?php echo JText::_('DIRECCION_USUARIO'); ?>  </label> <input type="text" name="direccion" value="<?= $this->usuario->direccion ?>"></td>
			
				<td> <label> <?php echo JText::_('CIUDAD_USUARIO'); ?>  </label> <input type="text" name="ciudad" value="<?= $this->usuario->ciudad ?>"></td>		
			</tr>
			<tr>
				<td colspan="4"> 
					<input type="submit" value="Editar" name="editar-inv" id="editar-inv" class="enviar" >
					<input type="reset" value="Borrar" name="reset" class="reset">
				 </td>
			</tr>

			</tbody>
		</table>
	</form>
</div>