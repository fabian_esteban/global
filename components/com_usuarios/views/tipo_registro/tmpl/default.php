<?php
// Joomla calls and runtimes
defined( '_JEXEC' ) or die();

?>

<h2> <?php echo JText::_('Escoja Tipo de Registro'); ?></h2>

<div id="componente-extractos">

	<ul>
		<li> <a href="index.php/registro-inversionistas"><?php echo JText::_('Registro Para Inversionistas Individuales'); ?> </a></li> 
		<li> <a href="index.php/registro-juridicas"><?php echo JText::_('Registro Para Personas Jurídicas'); ?> </a></li>
	</ul>
</div>