(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 184,
	height: 86,
	fps: 30,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.Símbolo8 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AAMAPIgDgBIgCABIgFAAIAAgDIAAAAQAEAAAAgHIAAgFQAAgFgBgCQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAAAgBAAIgBAAIgFACIAAAMIABAFIADABIAAAAIAAADIgFAAIgCgBIgDABIgFAAIAAgDIABAAQAEAAAAgHIAAgIIgBgFIgCgBIgBAAIgBABIAAgDIAGgBIAFgCIAAAFIAFgDIAEgCQAFAAACADQACACAAAGIABAOQAAAAAAABQAAAAABAAQAAAAAAAAQABAAABAAIAAAAIAAADg");
	this.shape.setTransform(62.2,3.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A548D").s().p("AgKATQgFgEABgHQgBgHAFgCQAFgFAFAAQAHAAAEAEQAFADgBAGQAAAHgEAFQgFAEgGAAQgGAAgEgEgAgFAAQgCACAAAEQAAAGADAEQACADACAAQAEAAABgDQACgCAAgFQAAgHgCgCQgDgCgCAAQgCAAgDACgAgDgKIAFgMIAGADIgJAKg");
	this.shape_1.setTransform(58.5,2.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0A548D").s().p("AADAWIgDAAIgCAAIgFAAIAAgDIABAAQAEAAAAgGIAAgLIAAgCIgCgBIgCAAIgBgCIAGgCIAFgCIgBAVIABAEIACABIABAAIAAADgAgCgOIgBgBIAAgCIAAgBIABgBIACgCIACACIACABIAAABIAAACIgCABIgCADg");
	this.shape_2.setTransform(55.7,2.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0A548D").s().p("AgFAPIgFgCIAAgFIAAgDIADgBIACAGQADACACAAIADgBQAAgBABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgCgFgDIgCgBQgDAAgCgBQAAAAgBgBQAAAAAAgBQAAAAAAgBQgBgBAAAAQAAgEAEgDQADgDAEAAIAIABIAAAEIABAEIgEABQAAgEgBgBQgCgCgDAAQAAAAAAAAQAAAAAAABQgBAAAAAAQgBAAAAABQAAAAgBAAQAAABAAAAQAAAAAAABQgBAAAAABQAAACAGACIAAAAIAHACQACADAAACQAAAEgEADQgDADgFAAg");
	this.shape_3.setTransform(53.3,3.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0A548D").s().p("AgCAOIgIABIAAAAIAAgDIABAAIACgBIABgFIgBgOIgCgBIAAAAIgBABIAAgDIAKgDIAAAFIAAADIAEgGQAAAAAAAAQABgBAAAAQABAAAAAAQABAAAAAAIADABIABADIgBACIgBABIgEgBIgDABIgCADIAAAIIAAAGIAEABIABAAIAAADg");
	this.shape_4.setTransform(50.6,3.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A548D").s().p("AgIALQgEgFAAgFQAAgGAEgEQAFgFAEAAQAGAAADAEQADAEAAAGIgSAAQAAAEADADQACADADAAIAFgBIAEgCIABADQgDADgDABIgHABQgDAAgFgEgAgCgIIgDAGIACAAIAJgBIgCgGQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_5.setTransform(47.6,3.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0A548D").s().p("AgCAPIgJgWQgBgDgCAAIgBAAIAAgEIAEABIAHAAIAEgBIAAAEIgDAAIgBABIAFAQIABgFIAFgLIAAgBIgCgBIgBAAIAAgDIADABIADAAIAEgBIACAAIAAAEIgBAAIgCAAIgCADIgKAWg");
	this.shape_6.setTransform(44.4,3.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0A548D").s().p("AANAPIgEgBIgDABIgEAAIAAgDIABAAQADAAAAgHIAAgFIgBgHQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAAAgBAAIgCAAIgDACIAAAMIAAAFIACABIABAAIAAADIgEAAIgEgBIgCABIgFAAIAAgDIAAAAQAFAAAAgHIAAgIIgBgFIgCgBIgBAAIgBABIAAgDIAFgBIAHgCIgBAFIAFgDIAEgCQAEAAADADQACACAAAGIABAOQAAAAAAABQAAAAAAAAQABAAAAAAQABAAAAAAIABAAIAAADg");
	this.shape_7.setTransform(40.9,3.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0A548D").s().p("AAEAVIgEgBIgDABIgGAAIAAgDIABAAIAEAAIABgBIAAgDIAAgbIAAgDIgBgBIgEAAIAAgDIARAAIAAADIgBAAIgDABIgBABIAAAYIAAAFIAAADIABABIADAAIACAAIAAADg");
	this.shape_8.setTransform(37.8,2.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0A548D").s().p("AgIALQgEgFAAgFQAAgGAEgEQAFgFAEAAQAGAAADAEQADAEAAAGIgSAAQAAAEADADQACADADAAIAFgBIAEgCIABADQgDADgDABIgHABQgDAAgFgEgAgCgIQgCADAAADIABAAIAJgBQAAgDgCgDQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_9.setTransform(33.2,3.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0A548D").s().p("AAFAVIAAgDIgFAEIgDABQgFAAgEgFQgDgDAAgHQAAgHAEgCQAFgFAGAAIACAAIADABIAAAAIAAgGIgBgGIgCgBIAAABIgCAAIAAgCIAMgDIAAAkIAAAEQAAAAAAAAQAAABABAAQAAAAAAAAQAAAAABAAIABAAIABgBIAAADIgFACIgGAAgAgGAAQgCABAAAGQAAAFACADQADADADAAIAFgCIAAgQIgDgBIgCgBQgDAAgDACg");
	this.shape_10.setTransform(29.8,2.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0A548D").s().p("AgFAPIgEgCIAAgFIgBgDIADgBQABAEABACQACACADAAQAAAAAAAAQABAAAAgBQABAAAAAAQABAAAAAAQAAgBABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgCgEgDIgDgBQgDAAgBgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAgBAAAAQAAgEADgDQAEgDADAAIAIABIABAIIgDABQgBgEgBgBQAAgBgBAAQAAAAgBAAQAAgBgBAAQgBAAgBAAQAAAAAAAAQAAAAAAABQAAAAgBAAQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAABQAAAAAAABQAAACAFACQAFACACAAQACACAAADQAAAEgEADQgEADgDAAg");
	this.shape_11.setTransform(24.7,3.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0A548D").s().p("AgIALQgEgFAAgFQAAgGAEgEQAFgFAEAAQAGAAADAEQADAEAAAGIgSAAQAAAEADADQACADADAAIAFgBIAEgCIABADQgDADgDABIgHABQgDAAgFgEgAgCgIQgCACgBAEIACAAIAJgBIgCgGQAAAAgBAAQAAgBgBAAQAAAAgBAAQAAAAgBAAQAAAAAAAAQAAAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_12.setTransform(21.6,3.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0A548D").s().p("AAEAPIgGgBIgIABIgBAAIAAgDIACAAIADgBIAAgFIAAgOIgCgBIgBAAIgBABIAAgDIAKgDIAAAFIAAADIADgGQABAAAAAAQABgBAAAAQABAAAAAAQABAAABAAIACABIABADIgBACIgBABIgEgBIgDABIgCADIAAAIIAAAGIADABIACAAIAAADg");
	this.shape_13.setTransform(18.8,3.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0A548D").s().p("AgKALQgFgEABgHQgBgFAFgEQAGgFAEAAQAHAAAEAEQAEAFAAAFQAAAGgEAFQgFAEgGAAQgGAAgEgEgAgEgHQgCACgBAFQABAEACAEQACADACAAQAEAAACgDQABgCAAgFQAAgEgBgEQgEgDgCAAQgDAAgBADg");
	this.shape_14.setTransform(15.7,3.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0A548D").s().p("AgFAPIgFgCIAAgFIAAgDIACgBIADAGQACACADAAQAAAAAAAAQABAAAAgBQABAAAAAAQABAAAAAAQAAgBABAAQAAAAAAgBQAAAAAAgBQABAAAAgBQAAgCgFgDIgCgBQgEAAgCgBQAAAAAAgBQAAAAgBgBQAAAAAAgBQAAgBAAAAQAAgEADgDQAEgDADAAIAIABIAAAEIABAEIgDABQgBgEgBgBQgCgCgDAAQAAAAAAAAQAAAAAAABQAAAAgBAAQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAABQAAAAAAABQAAACAEACIABAAIAHACQACACAAADQAAAEgEADQgEADgDAAg");
	this.shape_15.setTransform(12.4,3.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#0A548D").s().p("AgIALQgEgFAAgFQAAgGAEgEQAFgFAEAAQAFAAAEAEQADAEAAAGIgSAAQAAAEADADQACADADAAIAFgBIAEgCIABADQgDADgDABIgHABQgEAAgEgEgAgCgIQgCACgBAEIACAAIAJgBIgCgGIgEgBQAAAAAAAAQAAAAAAAAQgBABAAAAQgBAAAAABg");
	this.shape_16.setTransform(9.3,3.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0A548D").s().p("AgFAPIgEgCIAAgFIgBgDIADgBQAAAEACACQACACADAAQAAAAAAAAQABAAAAgBQABAAAAAAQABAAAAAAQAAgBABAAQAAAAAAgBQAAAAAAgBQAAAAAAgBQAAgCgEgDIgDgBQgDAAgBgBQgBAAAAgBQAAAAgBgBQAAAAAAgBQAAgBAAAAQAAgEADgDQAEgDADAAIAIABIABAIIgDABQgBgEgBgBQAAgBgBAAQAAAAgBAAQAAgBgBAAQgBAAgBAAQAAAAAAAAQAAAAAAABQAAAAgBAAQAAAAAAABQgBAAAAAAQAAABgBAAQAAAAAAABQAAAAAAABQAAACAFACQAFACACAAQACACAAADQAAAEgEADQgDADgEAAg");
	this.shape_17.setTransform(6.3,3.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#0A548D").s().p("AAEAVIAAgDIABAAIADAAIABgCIAAgBIAAgCIgDgHIgNAAIAAAAIgEAJIABACIAEABIAAAAIAAADIgQAAIAAgDIADgBQABAAAAAAQAAAAABgBQAAAAAAAAQAAgBABAAIAPgjIAFAAIAOAjIACACIACABIABAAIAAADgAgGACIAKAAIgEgPg");
	this.shape_18.setTransform(2.4,2.5);

	this.addChild(this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,63.9,4.8);


(lib.Símbolo7 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AgRAVQgCgCAAgEIABgEIACgDQACgDADgCIAEgCIAKgEIAAgEQAAgIgCgCQAAgBAAAAQAAgBgBAAQAAAAAAAAQAAgBAAAAIgEACIgDADIgCAEIAAADIgDACIgHADIAAgCIABgGIAFgGIAHgFQAEgBAEAAQAEAAADABIADAGQACAEAAAHIgBAIIABAJIAAADIACABIAAAAIACgDIABgBIABABIAAABIAAABQgBAFgCACQAAAAgBABQAAAAgBAAQAAAAgBAAQAAAAgBAAIgFgBQgCgCgCgEQgCAEgCACQgDACgGAAQgGAAgCgDgAgBADQgDACgCADIgBAFQAAABAAAAQAAABAAAAQAAABABAAQAAABAAAAQAAAAABABQAAAAAAAAQABAAAAAAQAAABABAAQADAAAAgCIADgFIAAgIIAAgEg");
	this.shape.setTransform(119.6,5.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0A548D").s().p("AgDAlIgFAAIgDAAIAAgEIAFAAIACgCIAAgEIABgCIAAgDIgBgFIABgCIgBgDIABgJIgBgDIgCgBIgFgBIAAgDQAHgBACgBIAEgFIADAAIAAAoIAAAEIAFABIACABIAAADgAgCgYQgCgCAAgDQAAgEABgBQABgBAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQADAAACACIABAEIAAAEQAAABgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAAAAAQgBABAAAAQgBAAAAAAQAAAAAAAAQAAgBAAAAQAAAAgBgBQgBAAAAgBg");
	this.shape_1.setTransform(115.4,3.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#0A548D").s().p("AgSAjIABgCIgBgFIABgBIgBg0IAAgBIgFgCIAAgDIAEgBIAIgFIAEAAIAAAjIAEgEIACgCIACgBIAEgBQAFAAADABIAGAFIAEAGIABAJIgBAHQAAAEgDAEQgBAEgDACQgCACgFACIgIABQgEAAgGgEIgDgCIgDAHIgDAAgAgDgCIgEACIABAaQAAADADACQACACACAAIAEgBIADgCIADgEIABgGIABgGQAAgFgCgFQgCgEgDgBQgCgCgEAAIgDABg");
	this.shape_2.setTransform(110.8,3.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0A548D").s().p("AgDAYIgHABIAAgEIAFgBIACgBIgBgcQAAgDgDgCQgBgCgEAAQgDAAgEACQAAABgBAAQgBABAAAAQAAABgBAAQAAABAAAAIAAABIAAABIAAAAIAAACIAAAXIAAABIAAABIADABIACABIABACIgBACIgLgBIgJABIgCAAIAAgEIABAAIAFgCIAAAAIABgaIgBgDIgGgDIAAgDIACAAIAIgDIAEgFIADAAIAAAJIAIgGIAHgBIAHACQACACACAEQACgDAEgCIAEgCIAGgBQAHAAADAFQAEAFAAAIIAAAVIABACIADACIACABIAAADIgEAAIgFgBIgMABIAAgDIAEgDIABgBIgBgbIgBgEIgDgDIgDgBQgDAAgDACQgCABgDAFIAAAYIABAEIAGADIAAACIAAABg");
	this.shape_3.setTransform(103.7,5.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#0A548D").s().p("AgLAVQgGgFgCgFQgCgGAAgFIABgHIADgHIAFgFIAGgEIAGgBQAKAAAFAHQAEAFACAEIABAIQAAAFgCAGQgDAGgFAEQgGAEgGAAQgGAAgFgEgAgFgRQgCACgBAFIgBALIAAAIIACAHIAEADQACACABAAQADAAADgDQACgCABgGIABgLQAAgKgDgFQgDgEgEAAQgDAAgCADg");
	this.shape_4.setTransform(96.6,5.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0A548D").s().p("AAIAlIgIAAIAAAAIgIAAIgDAAIAAgCIADgBIADgBQABgBAAgGIAAg0IgEgBIgCAAIAAgDIAIgDIAEgDIADAAIAABBIABAEQAAAAAAAAQAAAAAAAAQABAAAAAAQABAAAAAAIAEABIAAADg");
	this.shape_5.setTransform(92.4,3.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0A548D").s().p("AgLAVQgEgEgEgGQgCgEAAgHIABgHIADgHIAEgFIAHgEIAGgBQAKAAAGAHQADAEABAFIACAIQAAAHgCAEQgDAHgFADQgGAEgGAAQgFAAgGgEgAgFgRQgDADAAAEIgBALIABAIIACAHIADADIADACQAEAAACgDQADgDAAgFIABgLQAAgKgDgFQgDgEgEAAQgDAAgCADg");
	this.shape_6.setTransform(88.2,5.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#0A548D").s().p("AgEAlQgFgBgEgEQgFgCgEgHQgDgEgCgGQgBgFAAgHQAAgQAJgLQAKgLANAAIALABIAKACIACAKIABAIIgEAAQgEgIgGgEQgFgDgGgBQgFAAgFAIQgGAJAAAPIABAOIAEALIAGAFQADACAEAAQAGAAAEgDQAFgFAFgHIAEAAIgBAMIgBABIAAACQgDAEgGABQgFABgIAAQgFAAgEgBg");
	this.shape_7.setTransform(82,3.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#0A548D").s().p("AgJAXIgDgCIgCgBIgBgNIADAAIABAAIAAAAIADAHIADAEIAEACIABAAQADAAADgCQACgCAAgDIgBgDIgDgDIgLgEIgDgCIgDgDIgCgFQAAgFACgCQAAgDADgCQABgCADgBIAGgCIAGABIAGACIACAMIAAABIgDAAIgBAAIgBgCIgFgHQgCgCgCAAQgEAAgCACIgBAEQAAABAAAAQAAABAAABQABAAAAAAQAAABAAAAQADADADAAIAIAEQAEACACABQACAEAAAEQAAAHgFAEQgDAFgIAAg");
	this.shape_8.setTransform(73.5,5.2);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#0A548D").s().p("AgEAYIgGgDIgFgGIgCgGIgBgHIABgHIACgHIAGgHIAFgEIAEgBIAHABQADABABACQADABACADIACAFIABAIIgagBIAAACIAAAEIACAGQABADADABIAEAEIAFAAIAEgCIAFgDIACADQgDAFgGADQgFADgFAAgAgEgRIgDAJIAJABIADgBIAEAAIgBgGIgEgEQAAgBgBAAQAAAAgBgBQAAAAgBAAQAAAAgBAAQgBAAgDADg");
	this.shape_9.setTransform(68.8,5.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0A548D").s().p("AgDAlIgFAAIgDAAIAAgEIAFAAIACgCIAAgEIABgCIAAgDIgBgFIABgCIgBgDIABgJIgBgDIgCgBIgFgBIAAgDQAHgBACgBIAEgFIADAAIAAAoIAAAEIAFABIACABIAAADgAgCgYQgCgCAAgDQAAgEABgBQABgBAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQADAAACACIABAEIAAAEQAAABgBAAQAAABAAAAQAAAAgBABQAAAAgBAAIgDACQAAAAgCgDg");
	this.shape_10.setTransform(64.7,3.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0A548D").s().p("AgEAZQgDgDAAgHIABgZIgGAAIAAgDQAFgDADgDIAEgJIACAAIABADIAAAFIgBADIAAACIACAAIAFgBIADAAIAAADIAAADIgKAAIAAAUIAAAIIABADIADABIACAAIACgCIABgBIACACIAAABQgBADgDACQgEACgEAAQgBAAgEgEg");
	this.shape_11.setTransform(61.6,4.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0A548D").s().p("AgDAlIgFAAIgDAAIAAgEIAFAAIACgCIAAgDIAAgBIABgCIAAgDIgBgFIABgCIgBgDIABgJIgBgDIgCgBIgFgBIAAgDQAHgBACgBIAEgFIADAAIAAAoIAAAEIAFABIACABIAAADgAgCgYQgCgCAAgDQAAgEABgBQABgBAAAAQABAAAAgBQABAAAAAAQAAAAAAAAQADAAACACIABAEIAAAEQAAABgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAAAAAQgBABAAAAQgBAAAAAAQAAAAAAAAQAAgBAAAAQAAAAgBgBQgBAAAAgBg");
	this.shape_12.setTransform(58.2,3.8);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#0A548D").s().p("AgPAYIABgCIABgBIAFgCIAAgbIgBgBIgDgCQgBAAAAAAQAAAAAAAAQAAgBAAAAQAAAAAAAAIgBgDQAFgBACgBIAEgFIACgCIABAAIAAAMIADgFIADgDIADgCQABAAABAAQAAAAABAAQAAABABAAQAAAAABABQACACAAADQAAABAAAAQAAABgBABQAAAAAAABQAAAAgBABQAAAAAAABQgBAAAAAAQgBABAAAAQgBAAAAAAIgFgCIgCgBIAAAAIgCACIgBAGIAAATIABADIACABIADABIACABIABABIAAACIgMgBIgHABIgFAAg");
	this.shape_13.setTransform(54.9,5.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0A548D").s().p("AAIAXIAAgJIgCACIgGAGIgFABQgEAAgDgBIgFgFIgBgHIAAgZIAAgCIgCgBIgEgBIAAAAIAAgBIAAgCIABgBIAPAAIABAEIAAAfIACADQABABAAAAQABAAAAAAQABABAAAAQABAAAAAAIACgBIAFgEIACgDIAAgMIABgEIAAgEIgBgCIAAgEIgDgBIgDAAIgBgBIAAgDIADgBIAPAAIAAAKIAAAHIAAATIAAACIABABIAEgBIABAAIAAAEIgEAAIgLAFg");
	this.shape_14.setTransform(50.1,5.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0A548D").s().p("AgHAWIgIgGIgDgHIgBgIIABgHIAFgJQADgEAGgCQAEgCAEgBQAIAAAEADQADAEAAACQAAAEgBACQAAAAgBAAQAAAAgBABQAAAAgBAAQAAAAAAAAQgBAAgBAAQAAAAgBAAQAAgBgBAAQAAAAgBAAQgBgDgBgDIgCgDIgDgCQgDAAgEAFQgDAGgBAGQABAIAEAGQADAGAFAAIAEgBIAEgDIAEgEIABAAIACABQgCAHgEADQgFAEgIABQgEgBgEgCg");
	this.shape_15.setTransform(44.8,5.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#0A548D").s().p("AgEAYIgGgDQgDgCgBgEQgDgDAAgDIgBgHIAAgHIACgHIAGgHIAGgEIAEgBIAGABIAFADIAEAEIADAFIABAIIgagBIAAACIAAAEIACAGIADAEIAFAEIAFAAIAEgCIAEgDIADADQgDAFgFADQgGADgFAAgAgEgRQgDAEAAAFIAJABIADgBIADAAIgBgGQAAgDgDgBQAAgBgBAAQAAAAgBgBQAAAAgBAAQgBAAAAAAQgCAAgCADg");
	this.shape_16.setTransform(39.7,5.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#0A548D").s().p("AgJAlIgGgCIgDgBIgBgCIAAgPIAFAAIACAHIAEAFIAEACQADACABAAIAFgCQAEgBABgDQACgDAAgEQAAgDgDgFQgCgCgHgDQgKgFgCgCQgDAAgDgGQgCgEAAgFQAAgJAGgHQAFgGAJAAIAHABIAHACIABAGIABAMIgFAAQgCgIgCgDQgEgEgDgBQgFAAgCADQgDADAAAFQAAADACAEQADACAHAFIAHACIAHAFIAEAFIABAJQAAAGgCAFIgDAEIgEAFIgHADIgGABg");
	this.shape_17.setTransform(34.6,3.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#0A548D").s().p("AAJAlIgJAAIAAAAIgHAAIgEAAIAAgCIAHgCIABgHIAAgQIgBgDIAAghQgBAAAAAAQAAAAgBgBQAAAAgBAAQAAAAgBAAIgCAAIAAgDIAIgDIAEgDIADAAIAABBIABAEIACAAIAEABIAAADg");
	this.shape_18.setTransform(27.6,3.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#0A548D").s().p("AgRAVQgCgCAAgEIABgEIACgDQACgDADgCIADgCIALgEIAAgEQAAgHgCgDQAAgBAAAAQgBgBAAAAQAAAAAAAAQAAgBAAAAIgFACIgDADIAAAEIgBADIgDACIgDABIgEACIAAgCIABgGIAFgGIAGgFQAFgBAEAAQAEAAADABIADAGIACAcIAAADIACABIABAAIABgDIAAgBIACABIAAABIAAABIgCAHQgCABgEAAIgEgBIgEgGQgCAEgCACQgEACgFAAQgGAAgCgDgAgBADQgEACgCADIgBAFQABABAAAAQAAABAAAAQAAABAAAAQABABAAAAIADACQABAAAAgBQABAAAAAAQABAAAAgBQAAAAAAAAIACgFIABgIIgBgEg");
	this.shape_19.setTransform(23.9,5.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#0A548D").s().p("AgSAjIABgCIgBg7IgFgCIAAgDIAEgBIAIgFIAEAAIAAAjIADgEIAFgDIAEgBQAEAAADABIAHAFQADADABADIABAJIgBAHQAAAEgDAEIgEAGQgDADgEABIgIABQgDAAgHgEIgDgCIgDAHIgDAAgAgDgCIgEACIABAaQAAADADACQACACACAAIADgBIAEgCIADgEIABgGIAAgGQABgFgCgFQgBgDgEgCQgDgCgDAAIgDABg");
	this.shape_20.setTransform(18.2,3.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#0A548D").s().p("AgLAVQgFgFgCgFQgDgGAAgFIABgHIADgHIAEgFIAHgEIAGgBQAKAAAGAHQADAEABAFIACAIQAAAFgCAGQgEAHgFADQgFAEgGAAQgFAAgGgEgAgFgRQgCACgBAFIgBALIABAIIABAHIADADQADACABAAQADAAADgDQACgCABgGIABgLQAAgKgDgFQgDgEgEAAQgCAAgDADg");
	this.shape_21.setTransform(12.6,5.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#0A548D").s().p("AAIAlIgIAAIAAAAIgIAAIgDAAIAAgCIAGgCQABgBAAgGIAAg0IgEgBIgCAAIAAgDIAIgDIAEgDIADAAIAABBIABAEQAAAAAAAAQAAAAAAAAQABAAAAAAQABAAAAAAIAEABIAAACIAAABg");
	this.shape_22.setTransform(8.4,3.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#0A548D").s().p("AgIAlQgEgBgFgDIgIgIQgDgEgBgGQgDgIAAgHQAAgFAEgKQACgGAFgFQAFgGAGgCQAGgDAGAAIAOACIAJAEIABAOIAAACIgEAAIgFgJQgDgDgEgCIgIgCQgGAAgGAJQgGAIAAAPQAAAKACAGQADAHAEAEQAGAFAEAAIAGgBIADgCIABgDIABgJIgBgIIAAgCIgCAAIgEgCIgCgBIAAgBIAAgCIABAAIAJAAIALAAIADAAIAEAAIAAADIgBABIgDABIgCACIAAABIAAAPIgBAFIAAACQgCACgHACQgJACgHAAQgFAAgEgBg");
	this.shape_23.setTransform(3.3,3.9);

	this.addChild(this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,121.7,7.8);


(lib.Símbolo6 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AAcDIQhGgBg1gfQgsgZgYgsQgYgrAAg0QAAhSA4g6QAagcAqgRQAngPAngCQAygDBLANQAMADAFAEQAFAFACAGIARBPIgcAAQgdgogfgTQgggRgmAAQgtAAgeAkQgnAwAABVQAAA0AGAUQALAiARAXQAMAQAWAKQAZAOAfAAQAiAAAfgUQAfgUAdgpQAMgCASAAIgOBFQgCAJgEAHQgEAFgRAGIgnAIQgpAIgiAAIgFAAg");
	this.shape.setTransform(18.8,20);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,37.7,40.1);


(lib.Símbolo5 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AheC6QgKgDgFgEQgDgDgCgGIgBgMIgFgvQAAgHARAAQAGAAAEALQAEANAJAMQAJANAIAGQAPAKAMAEQANADATAAQAaAAAUgRQAWgTAAghQAAgcghgXQgJgGgwgZQgbgOgNgIQgWgMgLgPQgTgaAAghQAAglARgbQARgYAdgOQAegNAkAAQAcAAAkAPQAPAGAEAEQADACAAAGIACA4QAAAFgCACQgCABgHAAQgIAAgCgBQgBgBgBgFQgEgYgUgVQgSgWgfAAQgdAAgSARQgSARAAAbQAAAdAjAVIAhAVQARAIAbAQQAqAYAKAOQATAaAAAlQAAAggVAgQgOAWgaAOQgZANgcADIgUABQgmAAgrgNg");
	this.shape.setTransform(12.2,20);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,24.3,40);


(lib.Símbolo4 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AgyC/QgmgJgfgUQgjgVgUgeQgegsABg8QABg4AcgvQAcgvAzgcQAxgcA2AAQAzAAArALQAbAHATALQAHAEACAGIABAPIAAA1QgCAEgHAAQgGgBgDgEIgEgLQgMghgegRQgjgUgoAAQguAAgoAhQgmAggOAwQgMArAGAtQAHAvAYAkQAdAsAuAOQAZAIAbgDQAegDATgQQAUgQAAgjQAAglgKgKQgLgLghAAIgFAAQgIAAgCgCQgDgBAAgHQAAgJALgCIAeAAIBxAAIAOABQAGABAAAIQAAAHgCABQgDACgJABQgWABgCATQgCAGAAAiIACAyQAAAHgEAEQgDADgJAEQgLAFgXAHQgbAIgcAEQgUADgTAAQgkAAgkgJg");
	this.shape.setTransform(20.5,20);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,41,40.1);


(lib.Símbolo3 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AgmCuQgogHgfgSQgNgHgOgNQgsgmgFg/QgEg6AggzQAog/BIgVQAjgKAkgBQApgBAdALIARA8IgGADQAAAAgBAAQAAAAgBgBQAAAAgBgBQAAAAAAgBQgEgGgHgJQgMgNgPgJQglgWgrAHQgrAIghAeQgfAcgKApQgKAnAJApQAHAfAOAWQAcAsAuAUQAuAUAwgOQAggKARgWQAHgJgBgPQAAgHgEgSQgJgsgIgGQgFgEgSABQgRABgNADIgHABQgCAAgCgGQAAAAAAgBQAAgBAAAAQAAAAAAgBQAAAAABAAICagfQAFgBAAALIgjAHQgLABgBAJQAAAEADANIAHAhIAFAUQADAMAAAIQgBASgRAOQgKAHgXALQglARgjAGQgVAEgSAAQgRAAgRgDg");
	this.shape.setTransform(18.7,17.7);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,37.4,35.5);


(lib.Símbolo2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AhGFMIAAgbIAvABIgBgaIBrAAIgRhHIgTAEIgCgLQgUABgZgCQg3gEglgRQg4gZgcgfQgiglgRgvQgWg5AKhAQAKhAAmgyQAcgjAugbQAegSAZgHIgBgGIAKgCIgGgYIAIgCQgCgDAAgEQAAgDACgDQADgDAEAAQAEAAACADQADADAAADIgBAEIAIgCIAFAXIAJgBIACAIQAYgCAZAEQAWADARAFIgEALQgkgKgpAFIgQAEIAPA6IgWAHIgPg8QgeAJgjAUQgnAZgbAoQgcApgJAyQgDATABAYQAAAZAEAPQASBBAjAqQAkApA7AXQAXAJAcAEQASACAWgBQAUAAAIgDIgPhDIAVgCIAQBBQAegJAcgRQAYgOAYgYIADADQgYAZgSAOQgWAQgbAMIAEAOIgRAFIATBLIBeAAIAAAaIAtgBIAAAcg");
	this.shape.setTransform(24.3,33.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,48.7,66.7);


(lib.Símbolo1 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0A548D").s().p("AtYAJIAAgRIaxAAIAAARg");
	this.shape.setTransform(85.8,0.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,171.5,1.9);


(lib.Interpolación2 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","rgba(0,153,255,0.557)","rgba(255,255,255,0.388)","rgba(255,255,255,0)"],[0,0.42,0.533,1],-12.2,1.9,12.2,-1.8).s().p("AizlmIDygmIB0LzIjxAmg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-18,-39.7,36,79.5);


(lib.Interpolación1 = function() {
	this.initialize();

	// Capa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","rgba(0,188,253,0.557)","rgba(255,255,255,0.647)","rgba(66,66,66,0)"],[0,0.42,0.533,1],-12.2,1.9,12.2,-1.8).s().p("AizlmIDygmIB1LzIjyAmg");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-18,-39.7,36,79.5);


// stage content:
(lib.logo = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_340 = function() {
		this.gotoAndPlay(167)
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(340).call(this.frame_340).wait(1));

	// FlashAICB (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_166 = new cjs.Graphics().p("AjED2QgmgJgfgUQgjgWgVgeQgegsABg7QABg6AcgtQAdgvAygcQAygcA4AAQAyAAArALQAeAHAPALQAHAEACAGIABAOIAAA2QgCAEgHAAQgEgBgDgFIgEgKQgMghgegRQgkgUgnAAQgxAAgoAhQglAggOAwQgNApAHAvQAGAvAZAkQAeAsAtAOQAbAIAbgDQAegDATgQQATgQAAgjQAAglgJgKQgLgLghAAIgFAAQgIAAgCgCQgDgBAAgHQAAgJALgCIAeAAIBvAAIANABQAHABAAAIQAAAHgDABQgCACgKABQgVABgDATQgBAGAAAiIACAyQAAAHgFAEQgCADgJAEQgJAFgXAHQgbAIgcAEQgUACgTAAQgmAAgkgIgADHDwQgJgDgGgEQgDgDgBgGIgCgLIgEgvQAAgIAQAAQAGAAAEALQAEANAKANQAIANAJAFQAPAKAMAEQAMAEATAAQAdAAATgRQAXgUgBggQAAgdghgXQgJgGgxgZQgbgNgNgIQgWgOgLgQQgTgZAAggQAAglARgaQAQgZAegNQAegOAlAAQAdAAAjAPQAQAHAEADQADADAAAFIABA4QAAAGgBABQgCABgIAAQgIAAgBgBQgBgBgBgFQgFgYgTgVQgTgVgfAAQgeAAgTARQgRAQgBAcQABAdAiAUIAkATQAQAJAbAQQArAaAJANQAUAbAAAkQAAAggWAhQgOAWgaAOQgYAMgcAEIgXABQgmAAgrgOgALvD5QhHgBg2geQgsgZgZgsQgXgrAAg0QAAhSA4g6QAagcApgRQAngQApgCQAwgDBOAOQAMACAFAFQAFAEACAGIARBPIgcAAQgcgngggTQgggSgnAAQguAAgeAkQgoAxAABVQABAzAGAVQALAiARAWQAMAQAWALQAbANAeAAQAhAAAggUQAggUAcgpQAPgCAPAAIgNBGQgCAJgEAGQgEAGgRAFIgnAJQgpAHgjAAIgEAAg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(166).to({graphics:mask_graphics_166,x:91.1,y:25.5}).wait(175));

	// brillo
	this.instance = new lib.Interpolación1("synched",0);
	this.instance.setTransform(-24.4,35.7);
	this.instance._off = true;

	this.instance_1 = new lib.Interpolación2("synched",0);
	this.instance_1.setTransform(202.8,35.7);

	this.instance.mask = this.instance_1.mask = mask;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},166).to({state:[{t:this.instance_1}]},20).wait(155));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(166).to({_off:false},0).to({_off:true,x:202.8},20).wait(155));

	// asesores
	this.instance_2 = new lib.Símbolo8();
	this.instance_2.setTransform(150.2,92.9,1,1,0,0,0,31.9,2.4);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(148).to({_off:false},0).to({regX:32,regY:2.5,scaleX:1.91,scaleY:1.91,x:120.5,y:78.8},18,cjs.Ease.get(1)).wait(175));

	// nombre
	this.instance_3 = new lib.Símbolo7();
	this.instance_3.setTransform(120.6,60.3,1,1,0,0,0,60.9,3.9);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(121).to({_off:false},0).to({alpha:1},27,cjs.Ease.get(1)).wait(193));

	// c
	this.instance_4 = new lib.Símbolo6();
	this.instance_4.setTransform(204.1,31,1,1,0,0,0,18.8,20);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(102).to({_off:false},0).to({x:163.4,y:30.5},12,cjs.Ease.get(1)).wait(227));

	// s
	this.instance_5 = new lib.Símbolo5();
	this.instance_5.setTransform(198,31,1,1,0,0,0,12.2,20);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(85).to({_off:false},0).to({x:120.7},16).wait(240));

	// g
	this.instance_6 = new lib.Símbolo4();
	this.instance_6.setTransform(205.7,31,1,1,0,0,0,20.4,20);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(64).to({_off:false},0).to({x:76.4},21,cjs.Ease.get(1)).wait(256));

	// globo
	this.instance_7 = new lib.Símbolo3();
	this.instance_7.setTransform(29.4,31.2,0.167,0.167,0,0,0,18.6,17.7);
	this.instance_7.alpha = 0.27;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(49).to({_off:false},0).to({regX:18.7,scaleX:1,scaleY:1,alpha:1},15).wait(176).to({regX:18.4,scaleX:0.06,rotation:-11.9,x:28.3},20).to({regX:18.7,scaleX:0.99,rotation:0,x:29.4},20).wait(61));

	// base
	this.instance_8 = new lib.Símbolo2();
	this.instance_8.setTransform(-25.1,35.9,1,1,0,0,0,24.4,33.3);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(24).to({_off:false},0).to({x:26.9,y:36.3},25,cjs.Ease.get(1)).wait(292));

	// linea
	this.instance_9 = new lib.Símbolo1();
	this.instance_9.setTransform(190.1,70.6,0.061,1,0,0,0,85.8,0.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({scaleX:1,x:97.2,y:70.2},24,cjs.Ease.get(1)).wait(317));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(276.9,112.7,10.5,1.8);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;