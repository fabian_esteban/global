/**
* Contacto View
* version : 1.0
* package: Global.frontend
* package: Global.frontend.mvc
* author: Jhonny Gil
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var RegistrosView = {};

	// Extends my object from Backbone events
	RegistrosView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'submit #reg-juridicas-form': 'registroJuridica',
				'submit #reg-inversionistas-form': 'registroInversionista',
				'submit #edit-juridicas-form': 'editarJuridica',
				'submit #edit-inversionistas-form': 'editarInversionista'
			}

		,	initialize: function(){

				_.bindAll(
					this,
					'registroJuridica',
					'registroInversionista',
					'keyBoardUp'
				);

				this.usuarios = new RegistroModel();

				this.keyBoardUp();
				this.returnIp();

			}

			/*
			* Function que valida el formulario de registro para personas jurídicas
			*
			*/
		,	registroJuridica: function(e){

				e.preventDefault();

				console.log('here');
				
				var data = utilities.formToJson( '#reg-juridicas-form' )
				,	required = [ 
						'nombre',
						'email',
						'identificador',
						'password',
						'direccion',
						'telefono',
						'ciudad',
						'terminos'
					]
				,	errors = []
				,	_this = this;

				utilities.validateEmptyFields( required, data, errors );


				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if (! utilities.isEmail( data.email ))
					return utilities.showNotification( 'error', 'Digite un correo válido.', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( ! utilities.justNumbers( data.telefono ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

		        if( ! $("#terminos").is(':checked'))
		            return utilities.showNotification( 'error', 'Debe aceptar las condiciones legales.', 0 );
		        

				this.usuarios.set( data );

				this.usuarios.saveJuridica( this.onSendEmailForm, this.onError );
			}

		,	editarJuridica: function(e){

				e.preventDefault();
				
				var data = utilities.formToJson( '#edit-juridicas-form' )
				,	required = [ 
						'nombre',
						'email',
						'identificador',
						'direccion',
						'telefono',
						'ciudad'
					]
				,	errors = []
				,	_this = this;

				utilities.validateEmptyFields( required, data, errors );


				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if (! utilities.isEmail( data.email ))
					return utilities.showNotification( 'error', 'Digite un correo válido.', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( ! utilities.justNumbers( data.telefono ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				// if( ! utilities.justNumbers( data.celular ) )
				// 	return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

				// if( data.terminos == '2' )
				// 	return utilities.showNotification( 'error', 'Debe aceptar las condiciones legales.', 0 );

				this.usuarios.set( data );

				this.usuarios.editarJuridica( this.onSendEmailForm, this.onError );
			}			


			/*
			* Function que valida el formulario de registro para inversionistas libres
			*
			*/
		,	registroInversionista: function(e){

				e.preventDefault();
				
				var data = utilities.formToJson( '#reg-inversionistas-form' )
				,	required = [ 
						'nombre',
						'apellidos',
						'email',
						'identificador',
						'password',
						'fecha_nacimiento',
						'direccion',
						'telefono',
						'celular',
						'direccion',
						'ciudad'
					]
				,	errors = []
				,	_this = this;

				utilities.validateEmptyFields( required, data, errors );	


				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if (! utilities.isEmail( data.email ))
					return utilities.showNotification( 'error', 'Digite un correo válido.', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( ! utilities.justNumbers( data.telefono ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				if( ! utilities.justNumbers( data.celular ) )
					return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

		        if( ! $("#terminos").is(':checked'))
		            return utilities.showNotification( 'error', 'Debe aceptar las condiciones legales.', 0 );
		        

				this.usuarios.set( data );

				this.usuarios.saveInversionista( this.onSendEmailForm, this.onError );
			}
			/*
			* Fincion para editar los datos del registro 
			* de los usuarios inversionistas
			*/	
		,	editarInversionista: function(e){

				e.preventDefault();
				
				var data = utilities.formToJson( '#edit-inversionistas-form' )
				,	required = [ 
						'nombre',
						'email',
						'identificador',
						'direccion',
						'telefono',
						'ciudad'
					]
				,	errors = []
				,	_this = this;


				utilities.validateEmptyFields( required, data, errors );


				if( errors.length > 0 )
					return utilities.showNotification( 'error', 'Diligencie el formulario completamente', 0 );

				if (! utilities.isEmail( data.email ))
					return utilities.showNotification( 'error', 'Digite un correo válido.', 0 );

				if( data.password != data.confirmar_password )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( ! utilities.justNumbers( data.telefono ) )
					return utilities.showNotification( 'error', 'El campo teléfono solo debe tener números', 0 );

				if( ! utilities.justNumbers( data.celular ) )
					return utilities.showNotification( 'error', 'El campo celular solo debe tener números', 0 );

				this.usuarios.set( data );

				this.usuarios.editarInversionista( this.onSendEmailForm, this.onError );
			}			

			/*
			* Function execute when in backend send mail
			*
			*/	

		,	onSendEmailForm: function( data ){

				if (data.status == 500)
					return utilities.showNotification( 'error', data.message, 0 );

				utilities.showNotification('success',data.message, 2000, function(){
					window.location.reload();
				})

			}				

			/*
			* Función para el funcionamieto de el teclado virtual
			*
			*/	

		,	keyBoardUp: function(e) {

					$('.keyboard').keyboard({
						layout   : 'qwerty',
						lockInput: true // prevent manual keyboard entry
					});
	        }

	    ,	returnIp: function(){

	    		if ($('#ip').length > 0)
	    			$('#ip').val( myip );

	    	}


			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.RegistrosView = new RegistrosView();

})( jQuery, this, this.document, this.Misc, undefined );