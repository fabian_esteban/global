/**
* Compartir View
* version : 1.0
* package: odontoline.frontend
* package: odontoline.frontend.mvc
* author: Alejandro Suarez
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var RegistroView = {};

	// Extends my object from Backbone events
	RegistroView = Backbone.View.extend({

			el: $( '#member-registration' )

		,	events: {
				'click .validate': 'validatePasswords'
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this, 
					'validatePasswords'
				);

			}

			/**
			* Validate form and redirect the request to the different model
			*
			*/
			
		,	validatePasswords: function(e) {

				e.preventDefault();

				var password1 = $('#jform_password1').val()
				,	password2 = $('#jform_password2').val();

				if( password1 != password2 )
					return utilities.showNotification( 'error', 'Las contraseñas no coinciden', 0 );

				if( password1.length < 5 )
					return utilities.showNotification( 'error', 'La contraseña debe tener mas de 5 caracteres', 0 );

				if( ! $( 'input[name="accept-terms"]' ).is( ':checked' ) )
					return utilities.showNotification( 'error', 'Debe aceptar los términos y condiciones antes de registrarse', 0 );

				return $('#member-registration').submit();

			}
				

		,	onSendEmailForm: function( data ){

				console.log( data );

				if(data.status == 500)
					utilities.showNotification( 'error', data.message, 0 );

				utilities.showNotification( 'success', data.message, 0 );

			}

			/**
			* Initializes the datepicker
			*
			*/
		,	initDatePicker: function(){

				var fechas = JSON.parse( $( "#fechas" ).val() );

				utilities.initDatePicker( fechas );

			}				

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.RegistroView = new RegistroView();

})( jQuery, this, this.document, this.Misc, undefined );