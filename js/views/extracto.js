/**
* Información Corporativa View
* version : 1.0
* package: Global Security.frontend
* package: Global Security.frontend.mvc
* author: Jhonny Gil
* Creation date: Dec 2014
*
*
*/
( function( $, window, document, utilities ){

	// Create a var to manage the events
	var ExtractoView = {};

	// Extends my object from Backbone events
	ExtractoView = Backbone.View.extend({

			el: $( 'body' )

		,	events: {
				'click .find-extract': 'showModal',
				'click .accept-button-message': 'closeModal'
				
			}

		,	view: this

		,	initialize: function(){

				_.bindAll(
					this,
					'showModal'
				);


				google.load("visualization", "1", {packages:["corechart"]});

				google.setOnLoadCallback( this.renderDiagrama );

			}

			/**
			* Draw Diagrama EXtracto
			*
			*/
		,	renderDiagrama: function(){

				var data = google.visualization.arrayToDataTable([
					['Mes', 'Saldo disponible', {role: 'style'}],
					['Carteras colectivas',  1000, '#666666'],
					['Saldo disponible',  1170, '#76A9DA'],


				]);

				var options = {
					title: 'Composoción del portafolio',
					hAxis: {title: 'Año', titleTextStyle: {color: 'black'}},
					width: 450
				};

				var chart = new google.visualization.PieChart(document.getElementById('diagrama_charts'));

				chart.draw(data, options);

			}

		,	showModal: function (e) {
				 
				e.preventDefault();

				var html = new EJS( { url: url + 'js/templates/notfound.ejs' } ).render();

				var modal = {
					content: html
				};

				return utilities.showModalWindow( modal );


			}

		,	closeModal: function(e){

				e.preventDefault();

				utilities._closeModalWindow();
			}

			/**
			* Displays a message with an error
			*
			*/
		,	onError: function( XMLHttpRequest, textStatus, errorThrown ){

				console.log( "error :" + XMLHttpRequest.responseText );

				utilities.showNotification( 'error', 'Hubo un error interno en el sistema. Recargue la página e intente de nuevo.', 0 );

				return;
			}

	});

	window.ExtractoView = new ExtractoView();

})( jQuery, this, this.document, this.Misc, undefined );