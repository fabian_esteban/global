/**
* Sample Model
* version : 1.0
* package: global.frontend
* package: global.frontend.mvc
* author: Jhonny Gil
* Creation date: May 2014
*
* Description
*
*/
( function( $, window, document, utilities ){


	// Extends my object from Backbone model
	RegistroModel = Backbone.Model.extend({
	        
	        defaults: {
		            id: 0
		        ,	nombre: ''
		        ,	apellidos: ''
		        ,	email: ''
		        ,	password: ''
		        ,	nit: ''
		        ,	direccion: ''
		        ,	ciudad: ''
		        ,	telefono: ''
		        ,	web: '' 
		        ,	fecha_nacimiento:''
		        ,	celular:''
	        }

	    ,   initialize: function(){

	            
	        }

	    	/**
	    	* Saves the persona
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/
	    ,	saveJuridica: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_usuarios'
					,	task: 'usuarios.saveJuridica'
					,	usuario: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}

	    ,	saveInversionista: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_usuarios'
					,	task: 'usuarios.saveInversionista'
					,	usuario: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}	    	

	    	/**
	    	* Saves the persona
	    	*
	    	* @param { function } the callback function
	    	* @param { function } the callback error function
	    	*
	    	*/

 		,	editarJuridica: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_usuarios'
					,	task: 'usuarios.editarJuridica'
					,	usuario: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}
	    
 		,	editarInversionista: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_usuarios'
					,	task: 'usuarios.editarInversionista'
					,	usuario: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}



	    ,	editPersona: function( callback, errorCallback ){

	    		var _this = this;

	    		var aOptions = {
						dataType: "json"
					,	async: true
					,   success: callback
					,	error: errorCallback
				}
				
				,   aData = {
						option: 'com_usuarios'
					,	task: 'usuarios.editPersona'
					,	usuario: _this.attributes

				}

				// Pass the params to ajaxHandlerUpdated which will do the ajax request
				return utilities.ajaxHandler( aOptions, aData );
	    	}
	   });



})( jQuery, this, this.document, this.Misc, undefined );