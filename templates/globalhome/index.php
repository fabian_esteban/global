<?php
    $url = JFactory::getURI()->root();

    $suf_en="";
    if(JRequest::getVar('lang')=='en'){
        $suf_en="_en";
    }
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
	<head>
    	<meta charset="utf-8" />
		<jdoc:include type="head" />

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Coda:400,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,300,200,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/reset.css">
        <link rel="stylesheet" type="text/css" href="less/load-styles.php?load=home">

         <script src="http://code.jquery.com/ui/1.9.0/jquery-ui.min.js"></script>
         <link href="http://code.jquery.com/ui/1.9.0/themes/ui-darkness/jquery-ui.css" rel="stylesheet">
        <script type="text/javascript">
            var url = "<?php echo $url; ?>";
        </script>

        <!-- Scripts -->
		<script type="text/javascript">
         var x;
          x=jQuery(document);
          x.ready(inicio);
          function inicio(){
            var ancho= window.innerWidth;
            var margen= (1900-ancho)/2;
            var x;
            x=jQuery("#banner");
            x.css("margin-left","-"+margen+"px");
          }
		</script>

	</head>

    <body onresize="inicio()">
        <header>
            <div id="header-center">
                <div class="header-top">
                    <div class="logo-global">
                        <iframe width="204" height="96" frameborder="0" src="html5/logo.html" style="background-color:transparent" scrolling="no"></iframe>
                    </div>
                    <div class="left-header">
                        <div class="idiomas">
                            <?php 
                                if(JRequest::getVar('lang')=='es'){
                                ?>
                                    <p><a href="index.php/en/home">Ing /</a> Esp</p>
                                <?php 
                                }else{
                                ?>
                                    <p>Ing / <a href="index.php/es/">Esp</a></p>
                                <?php 
                                }
                            ?>
                        </div>
                        <div class="redes">
                            <jdoc:include type="modules" name="redes"  style="xhtml" />
                        </div>
                    </div>
                </div>
                <nav>
                    <jdoc:include type="modules" name="menu"  style="xhtml" />
                </nav>
            </div>
        </header>

        <div id="banner">
            <jdoc:include type="modules" name="banner"  style="xhtml" />
        </div>

        <div class="botones-lateral">
            <div class="mail-marketing">
                <jdoc:include type="modules" name="mail"  style="xhtml" />
            </div>
            <div class="chat">
                <a title="Chat" href="javascript:void(window.open(url+'chat/chat.php?code=U0VSVkVSUEFHRQ__','','width=590,height=610,left=0,top=0,resizable=yes,menubar=no,location=yes,status=yes,scrollbars=yes'))"><img src="images/inicio/icono-chat.png" alt="icono-chat" /></a>
            </div>
            <div class="videos">
                <jdoc:include type="modules" name="videos"  style="xhtml" />
            </div>
        </div>

        <div class="logos-lateral">
            <jdoc:include type="modules" name="logos-lateral"  style="xhtml" />
        </div>

        <section class="middle">

            <div class="destacados">
                <jdoc:include type="modules" name="destacados"  style="xhtml" />
            </div>
            <div class="login">
                <jdoc:include type="modules" name="login"  style="xhtml" />
            </div>

        </section>

        <main>
            <section class="main-top">

                <div class="noticias">
                    <jdoc:include type="modules" name="noticias"  style="xhtml" />
                </div>
                <div class="twitter">
                    <jdoc:include type="modules" name="twitter"  style="xhtml" />
                </div>
                <div class="indicadores">
                    <?php 
                        if(JRequest::getVar('lang')=='es'){
                        ?>
                            <h3>Indicadores Económicos</h3>
                        <?php 
                        }else{
                        ?>
                            <h3>Economic Indicators</h3>
                        <?php 
                        }
                    ?>
                    <div id="bgBody">
                        <script type="text/javascript">
                        // <![CDATA[
                        var bgHost = "http://www.applab.in/";
                        var bgType = "CO-19284-1";
                        var bgIndi = "6|7|9|10|3|5";
                        (function(d){ var f = bgHost+ "apps/indicators/"+bgType+"/"+bgIndi+"/functions.js"; d.write(unescape("%3Cscript src='"+f+"' type='text/javascript'%3E%3C/script%3E")); })(document);
                        // ]]>
                        </script>
                    </div> 
                </div>

            </section>

            <div class="like">
                <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FGlobalSecuritiesColombia&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>

                <a href="https://twitter.com/GSCColombia" class="twitter-follow-button" data-show-count="true" data-show-screen-name="false">Follow @GSCColombia</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>
        </main>

        <section class="mapa-navegacion">
            <jdoc:include type="modules" name="mapa-navegacion"  style="xhtml" />
        </section>

        <footer>
            <address>
                <jdoc:include type="modules" name="direccion"  style="xhtml" />
            </address>
            <div class="creditos">
                <div class="copy">
                    <span class="sainet">
                        <a target="_blank" href="http://www.creandopaginasweb.com">
                            <?php 
                                if(JRequest::getVar('lang')=='es'){
                                ?>
                                    Agencia de diseño web
                                <?php 
                                }else{
                                ?>
                                    Web design agency
                                <?php 
                                }
                            ?>
                            <img alt="Diseño de paginas web" src="http://www.creandopaginasweb.com/theme/img/logoverde.png">
                        </a>
                    </span>
                </div>
            </div>
        </footer>
        <script type="text/javascript" src="js/load-scripts.php"></script>
	</body>
</html>
