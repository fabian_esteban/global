<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	com_solicitud
 */

// No direct access.
defined('_JEXEC') or die;
jimport( 'joomla.environment.uri' );


?>
<table class="adminlist">
	<thead>
		<tr>
			<th class="nowrap">Id</th>
			<th class="nowrap">Nombre</th>
			<th class="nowrap">Apellidos</th>
			<th class="nowrap">Nombre de Usuario</th>
			<th class="nowrap">Email</th>
			<th class="nowrap">Fecha de registro</th>
			<th class="nowrap">Cédula / NIT</th>
			<th class="nowrap">Fecha Nacimiento</th>
			<th class="nowrap">Teléfono</th>
			<th class="nowrap">Celular</th>
			<th class="nowrap">Dirección</th>
			<th class="nowrap">Ciudad</th>
			<th class="nowrap">Sitio Web</th>
			<th class="nowrap">Tipo Usuaruo</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($this->items as $i => $item) :
	?>
		<tr class="row<?php echo $i % 2; ?>">
			<td class="center"><?php echo $item->id?></td>
			<td class="center"><?php echo $item->name?></td>
			<td class="center"><?php echo $item->apellidos?></td>
			<td class="center"><?php echo $item->username?></td>
			<td class="center"><?php echo $item->email?></td>
			<td class="center"><?php echo $item->registerDate?></td>
			<td class="center"><?php echo $item->username?></td>
			<td class="center"><?php echo $item->fecha_nacimiento?></td>
			<td class="center"><?php echo $item->telefono?></td>
			<td class="center"><?php echo $item->celular?></td>
			<td class="center"><?php echo $item->direccion?></td>
			<td class="center"><?php echo $item->ciudad?></td>
			<td class="center"><?php echo $item->web?></td>
			<td class="center"><?php echo ( $item->tipo == 2 ) ? 'Juridica': 'Inversionista';?></td>

		</tr>
	<?php 
		
	endforeach; 
	?>
	</tbody>
</table>